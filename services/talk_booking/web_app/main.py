import pathlib
import uuid

from fastapi import Depends, FastAPI
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database import talk_request_db
from models import TalkRequest

from .api_requests import AcceptTalkRequest, RejectTalkRequest, SubmitTalkRequest
from .api_responses import TalkRequestDetails, TalkRequestList
from .config import load_config

app = FastAPI()
app_config = load_config()


def get_db_session():
    engine = create_engine(app_config.SQLALCHEMY_DATABASE_URI, echo=False)
    db = sessionmaker(bind=engine)()

    try:
        yield db
    finally:
        db.close()


@app.get("/health-check/")
def health_check():
    return {"message": "OK"}


@app.post("/request-talk/", status_code=201, response_model=TalkRequestDetails)
def request_talk(
    submit_talk_request: SubmitTalkRequest, db_session=Depends(get_db_session)
):
    talk_request = TalkRequest(
        id=str(uuid.uuid4()),
        event_time=submit_talk_request.event_time,
        address=submit_talk_request.address,
        topic=submit_talk_request.topic,
        status="PENDING",
        duration_in_minutes=submit_talk_request.duration_in_minutes,
        requestor=submit_talk_request.requestor,
    )
    talk_request = talk_request_db.save(db_session, talk_request)
    return talk_request


@app.get("/talk-requests/", status_code=200, response_model=TalkRequestList)
def talk_requests(db_session=Depends(get_db_session)):
    return {
        "results": [
            talk_request.dict() for talk_request in talk_request_db.list_all(db_session)
        ]
    }


@app.post("/talk-request/accept/", status_code=200, response_model=TalkRequestDetails)
def accept_talk_request(accept_talk_request_body: AcceptTalkRequest):
    return {
        "id": accept_talk_request_body.id,
        "event_time": "2021-10-03T10:30:00",
        "address": {
            "street": "Sunny street 42",
            "city": "Sunny city 42000",
            "state": "Sunny state",
            "country": "Sunny country",
        },
        "topic": "FastAPI with Pydantic",
        "status": "ACCEPTED",
        "duration_in_minutes": 45,
        "requestor": "john@doe.com",
    }


@app.post("/talk-request/reject/", status_code=200, response_model=TalkRequestDetails)
def reject_talk_request(reject_talk_request_body: RejectTalkRequest):
    return {
        "id": reject_talk_request_body.id,
        "event_time": "2021-10-03T10:30:00",
        "address": {
            "street": "Sunny street 42",
            "city": "Sunny city 42000",
            "state": "Sunny state",
            "country": "Sunny country",
        },
        "topic": "FastAPI with Pydantic",
        "status": "REJECTED",
        "duration_in_minutes": 45,
        "requestor": "john@doe.com",
    }
