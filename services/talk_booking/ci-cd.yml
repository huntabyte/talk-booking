service-talk-booking-code-quality:
  stage: test
  image: registry.gitlab.com/huntabyte/talk-booking:cicd-python3.9-slim
  before_script:
    - cd services/talk_booking/
    - poetry install
  script:
    - poetry run flake8 .
    - poetry run black . --check
    - poetry run isort . --check-only --profile black
    - poetry run bandit .
    - poetry run safety check
  only:
    refs:
      - merge_requests
      - main
    changes:
      - services/talk_booking/**/*

service-talk-booking-tests:
  stage: test
  image: registry.gitlab.com/huntabyte/talk-booking:cicd-python3.9-slim
  before_script:
    - cd services/talk_booking/
    - poetry install
  script:
    - poetry run python -m pytest --junitxml=report.xml --cov=./ --cov-report=xml tests/unit tests/integration
  after_script:
    - bash <(curl -s https://codecov.io/bash)
  artifacts:
    when: always
    reports:
      junit: services/talk_booking/report.xml
  only:
    refs:
      - merge_requests
      - main
    changes:
      - services/talk_booking/**/*


# Builds & Pushes App Docker image to AWS ECR (TEMPLATE)
.service-talk-booking-docker-image:
  image: registry.gitlab.com/huntabyte/talk-booking:cicd-docker
  stage: docker
  services:
    - docker:19.03.0-dind
  before_script:
    - cd services/talk_booking/
    - poetry export --without-hashes --with-credentials -f requirements.txt > requirements.txt
    - aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 842544287990.dkr.ecr.us-east-1.amazonaws.com
  script:
    - docker build --pull -t "$DOCKER_IMAGE_TAG" .
    - docker push "$DOCKER_IMAGE_TAG"

# Builds & Pushes App Docker image to AWS ECR (Development)
service-talk-booking-docker-image-development:
  extends: .service-talk-booking-docker-image
  variables:
    DOCKER_IMAGE_TAG: 842544287990.dkr.ecr.us-east-1.amazonaws.com/talk-booking-dev:development-$CI_COMMIT_SHA
  only:
    refs:
      - merge_requests
      - main
    changes:
      - services/talk_booking/**/*

# Builds & Pushes App Docker image to AWS ECR (Production)
service-talk-booking-docker-image-production:
  extends: .service-talk-booking-docker-image
  variables:
    DOCKER_IMAGE_TAG: 842544287990.dkr.ecr.us-east-1.amazonaws.com/talk-booking-prod:production-$CI_COMMIT_SHA
  except:
    - branches
  only:
    refs:
      - /^talk-booking-[0-9]+(?:.[0-9]+)+$/
    changes:
      - services/talk_booking/**/*

# Deploy job Template
.service-talk-booking-deploy:
  image: registry.gitlab.com/huntabyte/talk-booking:cicd-python3.9-slim
  stage: deploy
  before_script:
    - cd services/talk_booking/
    - pip install boto3
  script:
    - python deploy.py --cluster_name $ENVIRONMENT_NAME --service_name $ENVIRONMENT_NAME --new_image_uri $DOCKER_IMAGE_TAG

# Deploy Application (Development)
service-talk-booking-development:
  extends: .service-talk-booking-deploy
  variables:
    ENVIRONMENT_NAME: talk-booking-dev
    APP_ENVIRONMENT_NAME: development
    DOCKER_IMAGE_TAG: 842544287990.dkr.ecr.us-east-1.amazonaws.com/talk-booking-dev:development-$CI_COMMIT_SHA
  only:
    refs:
      - merge_requests
      - main
    changes:
      - services/talk_booking/**/*

# Deploy Application (Production)
service-talk-booking-production:
  extends: .service-talk-booking-deploy
  variables:
    ENVIRONMENT_NAME: talk-booking-prod
    APP_ENVIRONMENT_NAME: production
    DOCKER_IMAGE_TAG: 842544287990.dkr.ecr.us-east-1.amazonaws.com/talk-booking-prod:production-$CI_COMMIT_SHA
  except:
    - branches
  only:
    refs:
      - /^talk-booking-[0-9]+(?:.[0-9]+)+$/
    changes:
      - services/talk_booking/**/*


# End to End tests
service-talk-booking-e2e:
  stage: e2e
  image: registry.gitlab.com/huntabyte/talk-booking:cicd-python3.9-slim
  before_script:
    - cd services/talk_booking/
    - poetry install
  script:
    - poetry run python -m pytest tests/e2e
  only:
    refs:
      - merge_requests
      - main
    changes:
      - services/talk_booking/**/*